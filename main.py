import os


class ConsoleColours:
    FAIL = '\033[91m'
    NORMAL = '\033[0m'


class Console:
    @staticmethod
    def cls() -> None:
        os.system('cls' if os.name == 'nt' else 'clear')

    @staticmethod
    def print_error(message: str or object, clear: bool = False) -> None:
        if isinstance(message, str):
            if clear is True:
                Console.cls()

            print(f'{ConsoleColours.FAIL}{message}{ConsoleColours.NORMAL}')


class ImprovedPCInput:
    @staticmethod
    def get_string(question: str) -> str:
        while True:
            question = question.rstrip('\n') + '\n'
            answer = input(question).strip()

            if not answer:
                Console.print_error('Input mag niet leeg zijn!', True)
            else:
                return answer


def ask_number(question: str):
    number = ImprovedPCInput.get_string(question)
    return number


def run_program():
    company_number = ask_number('Geef een Belgisch Ondernemingsnummer in om te controleren:')

    print(company_number)


if __name__ == '__main__':
    run_program()
